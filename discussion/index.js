// console.log("Hello");

// Arithmetic Operators
/*
	+ sum
	- subtraction
	* multiplication
	/ division
	% modulo (returns the remainder)
*/
let x = 45;
let y = 28;

let sum = x + y;
console.log("Result of addition: " + sum);

let difference = x - y;
console.log("Result of subtraction: " + difference);

let product = x * y;
console.log("Result of product: " + product);

let quotient = x / y;
console.log("Result of quotient: " + quotient);

let mod = y % x;
console.log("Result of Modulo: " + mod);

// Assignment Operator
// Basic assignment operator (=)
let assignmentNumber = 8;

// Arithmetic Assignment Operator
// Addition Assignment Operator (+=)
// Subtraction Assignment Operator (-=)
// Multiplication Assignment Operator (*=)
// Division Assignment Operator (/=)
// Modulo Assignment Operator (%=)


assignmentNumber = assignmentNumber + 2;
console.log("Result of addition operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("")

// PEMDAS 
/*
	P - Parenthesis
	E - Exponent
	M - Multiplication
	D - Division
	A - Addition
	S - Subtraction
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas: " + mdas);

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas: " + pemdas);

// Increment and Decrement
// Opertors that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to
let z = 1;
console.log(z);

let increment = ++z;// increment = 1 + z 
// The value "z" is added by a value of one before returning the value and storing it in variable "increment"
console.log("Pre-incremet: " + increment);
// result: increment - 2
// The value of "z" was also increased even though we didn't implicitly specify any value reassignment
console.log("Value of z: " + z);
// result: z - 2

// The value of "z" is returned and stored in the varible "increment" then the value of z is increased by one
increment = z++;
// increment = 2 plus 1 (z+1) 
console.log("Post-increment: " + increment);
// The value of "z" was increased again reassigning the value to 3
console.log("Value of z: " + z);

// The value of "z" is decresed by a value of one before returning the value and storing it in the variable "decrement"
let decrement = --z
// The value of z is at 3 before it was decremented. Result: 2
console.log("Pre-decrement: " + decrement);
console.log("Value of z: " + z);

// The value of z is retuend and stored in the variable "decrement" then the value of z is decreased by one
decrement = z--;
// The value of z at 2 before it was decremented
console.log("Post-decrement: " + decrement);
// The value of z was decreased reassigning the value of 1
console.log("Value of z: " + z);


// Type Coercion
/*
	Type coercion is the automatic or implicit conversion of values fron one data type tp another
		- this happend when operations are performed on different data types that would normally not be possible and yield irregular results
		- values are automatically converted from one data type to another in order to resolve operations

 */
let numA = '10';
let numB = 12;

/*
	adding/concatenating a string and a number will result in a string

 */
let coercion = numA + numB;
console.log(coercion);// result:1012 - concatenation
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);

let numE = true + 1;
console.log(numE);//result : 1 + 1 = 2
/*
	The boolean "true" is also associated with the value of 1
 */
let numF = false + 2;
console.log(numF);
numG = true + 5;
console.log(numG);

// Equality Operator (==)
/*
	- checks whether the operance are equal/have the same content
	- attempts to CONVERT AND COMPARE operand of different data types
 */
console.log(1 == 1);//result:true
console.log(1 == 2);//result:false
console.log(1 == '1');//result:true
console.log('jungkook' == 'jungkook');//result:true
console.log('Jungkook' == 'jungkook');//result:false
console.log ('a' == 'A');//(61 == 41)

// Strict Equality (===)
/*
	- checks whether the operands are equal/have the same content
	- ALSO COMPARE the data type of 2 values
	- JavaScript is a loosely typed language meaning that values of different dta tyoes can be stored in variables
	- in combination with type coercion, this sometime create problems within our code (e.g Java, Typescript)
	- Strict equality operators are better to use in most cases to ensure that data types provided are correct
 */
console.log (1 === 1);//result: true
console.log(1 === '1');//result: false

let johnny = 'johnny';
console.log('johnny' === johnny);
console.log(false === 0);

// Inequality Operator !=
/*
 - checks whether the operands are not equal/have different content
 - attempts the CONVERT and COMPARE operands of different data type
 */
console.log(1 != 1);//false
console.log(1 != 3);//true
console.log(1 != '1');//false
console.log('jungkook' != 'jungkook');//false
console.log('Jungkook' != 'jungkook');//true

// Strict Inequality !==
/*
	- checks whether the operands are not equal/have the same content
	- also COMPARES the data type of 2 values
 */
console.log(1 !== 1);//false
console.log(1 !== '1');//true

// Relational Operator
/*
	Some comparison operators check whether one value is greater than or less than to the other value.
	Like in equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement
 */

let a = 50;
let b = 65;

// GT or Greater than (>)
let isGreaterThan = a > b;
console.log(isGreaterThan);//false

// LT or Less than (<)
let isLessThan = a < b;
console.log(isLessThan);

// GTE or Greater than or equal to (>=)
let GTE = a >= b;
console.log(GTE);
// LTE or Less than or equal to (<=)
let LTE = a <= b;
console.log(LTE);

// forced coercion to change string to a number
let numStr = '30';
console.log(a > numStr);//true

let str = 'twenty';
console.log(b >= str);//false

// Logical Operator
/*
  AND operator (&&) 
  - returns TRUE if all operands are true

  p 	q 		p && q
  f     f 		false
  f     t       false
  t     f       false
  t     t       true
  
  OR operator (|| - Double pipe)
  - return TRUE if one of the operands are true
  p 	q      p || q
  f     f      false
  f     t      true
  t     f      true
  t     t      true
 */

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true; 

let authorization1 = isAdmin && isRegistered;// f && t = false
console.log(authorization1);

let voteAuthorization = isLegalAge && isRegistered;
console.log(voteAuthorization);

let adminAccess = isAdmin && isLegalAge && isRegistered;
console.log(adminAccess);//false

let random = isAdmin && false;
console.log(random);

let requiredLevel = 95;
let requiredAge = 18;

let gameTopPlayer = isRegistered && requiredLevel === 25;
console.log(gameTopPlayer);

let gamePlayer = isRegistered && isLegalAge && requiredLevel >= 25;
//console.log(requiredLevel >= 25);
console.log(gamePlayer);

let userName = 'kookie2022';
let userName2 = 'gamer01';
let userAge = 15;
let userAge2 = 26;

let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1);

let registration2 = userName.length > 8 && userAge2 >= requiredAge;
console.log(registration2);

// or
let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered || userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement);

let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin);

// Not operator ! - it returns opposite
let guildAdmin2 = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin2);

console.log(!isRegistered);

/*Mini Activity*/
console.log("-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");




